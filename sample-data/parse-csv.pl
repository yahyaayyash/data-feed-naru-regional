#!/usr/bin/perl -wl
use strict;
use Text::CSV;
use Data::Dumper;

my $csv = Text::CSV->new;
my $filename = 'LinkReport_2018-12-15.csv';
open my $fh, "<:encoding(utf8)", $filename or die "$filename: $!";
my $rownum = 0;
my %data;

while ( my $row = $csv->getline( $fh ) ) {
	next unless $rownum++;
	#print Dumper $row;
	my @record;
	($record[0]) = ($row->[0] =~ /([\d\.]+)/);

	$row->[2] = 'east java'    if $row->[2] =~ /JAWA TIMUR/i ;
	$row->[2] = 'central java' if $row->[2] =~ /JAWA TENGAH/i ;
	$row->[2] = 'west java'    if $row->[2] =~ /JAWA BARAT/i ;
	$row->[2] = 'bali nusra'   if $row->[2] =~ /balinusra/i ;

	$data{$record[0]} ||= uc $row->[2];
	#print Dumper $record;
	#print join(",", @record)
}

for (keys %data) {
	print join(",", $_, $data{$_})
}
