package com.softegra.daemon

import java.sql.Date
import java.text.SimpleDateFormat
import java.util.Calendar

import com.softegra.common.InitSession
import com.softegra.util.ClickHouseUtil

import scala.collection.mutable.ListBuffer

object Naru3Daemon {
  private var isTimerUp = false

  def runNaru3Thread(): Unit = {
    if (!isTimerUp) {
      isTimerUp = true
      new Thread(new Naru3Thread()).start()
    }
  }
}

class Naru3Thread extends Runnable {
  def run: Unit = {
    while (true) {
      println(s"Start treat!! ${new java.text.SimpleDateFormat("d-MMM-yyyy_HH-mm").format(new java.util.Date(System.currentTimeMillis()))}")

      //init
      var nioStrQuery = new ListBuffer[String]()
      var timeStringList = new ListBuffer[String]()

      /**
        * Run
        *
        * */
      try {
        getNaru3()
        insertNaru3()
      } catch {
        case e: Throwable => e.printStackTrace
      }


      def getNaru3(): Unit = {
        println("get file naru3")
        val cal = Calendar.getInstance()
        //ssh
        val address = "10.251.166.149"
        val username = "nio"
        val password = "nio123"
        val session = InitSession.init(address, username, password)
        val listCommand = new ListBuffer[String]()
        for(i <- 1 to 7){
          val millis = cal.getTimeInMillis - ( i * (1000 * 60 * 30))
          val minuteFormat = new SimpleDateFormat("yyyyMMdd-HHmm")
          val deeto = minuteFormat.format(millis)
          val year = deeto.substring(0,4)
          val month = deeto.substring(4,6)
          val day = deeto.substring(6,8)
          val hour = deeto.substring(9,11)
          val minute = deeto.substring(11,13)
          val m = if(minute.toInt < 30) "00" else "30"
          val timeString = s"$year$month$day-$hour$m"
          val naru3TimeString = getTimeStringNaru3()

          timeStringList.append(timeString)

          if(!naru3TimeString.contains(timeString)){
            println(s"donwloading naru_3_tsel-$timeString.csv .....")
            val naru3 = s"sftp nio-naru@10.40.92.31:naru_3_tsel-$timeString.csv /home/nio/naru3/naru_3_tsel-$timeString.csv"
            listCommand.append(naru3)
          } else {
            println(s"ignoring naru_3_tsel-$timeString.csv .....")
          }
        }
        listCommand.append("cd /home/nio/naru3/;ls")
        val commands = listCommand.mkString(";")
        println("get list file", commands)

        //remove file. empty src folder
        println("remove inside src folder")
        val removeCmd = "rm -f /home/nio/naru3/*"
        ClickHouseUtil.run2(session, removeCmd, (line: String) => println(line))

        //get file
        println("downloading file")
        ClickHouseUtil.run2(session, commands, (line: String) => println(line))
        println("get selesai")
      }

      def insertNaru3(): Unit = {
        println("insert nio")
        //ssh
        val address = "10.251.166.149"
        val username = "nio"
        val password = "nio123"
        val table = "init.naru3"
        val session = InitSession.init(address, username, password)

        val cal = Calendar.getInstance()
        var listCommand = new ListBuffer[String]()
        val listFile = new ListBuffer[String]()

        //get list file
        ClickHouseUtil.run2(session, "cd /home/nio/naru3/;ls", (line: String) => listFile.append(line))
        println("isi listFile",listFile)

        //create cat command
        listCommand.append("cd /home/nio/src/")
        listFile.foreach(x => listCommand.append(s"cat /home/nio/naru3/"+x))
        val commands = listCommand.mkString(";")

        //read file
        println("start insert")
        ClickHouseUtil.run2(session, commands, (line: String) => readLine(line, table))
        ClickHouseUtil.insertQuery(nioStrQuery.mkString(" "), table)
        nioStrQuery = new ListBuffer[String]()
        println("insert selesai")

        //remove file
        val removeCmd = "rm -f /home/nio/naru3/*"
        ClickHouseUtil.run2(session, removeCmd, (line: String) => println(line))
      }

      def getTimeStringNaru3(): List[String] ={
        val sql =
          """
            |select distinct(timestring)
            |from init.naru3;
          """.stripMargin
        ClickHouseUtil.selectQuery(sql).map(x => x("timestring").toString)
      }


      def readLine(x: String, table: String): Unit = {
        if(!x.contains("application_name")){
          val lineSplit = x.split(',').toList
          val timestamp = lineSplit(0)
          val region = lineSplit(1)
          val application = lineSplit(2)
          val subs = lineSplit(3)
          val volume = lineSplit(4)
          nioStrQuery.append(s"('$timestamp', '$region', '$application', $subs, $volume)")
        }
      }
      println(s"Thread selesai !! ${new java.text.SimpleDateFormat("d-MMM-yyyy_HH-mm").format(new java.util.Date(System.currentTimeMillis()))}")
      val sleepTime = 1000 * 60 * 30
      Thread.sleep(sleepTime)
    }
  }
}
