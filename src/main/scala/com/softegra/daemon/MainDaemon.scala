package com.softegra.daemon

object MainDaemon {

  private var isTimerUp = false

  def runMainThread(): Unit = {
    if (!isTimerUp) {
      isTimerUp = true
      new Thread(new Naru3Thread()).start()
    }
  }

}
