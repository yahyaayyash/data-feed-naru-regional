package com.softegra.util

import java.sql.ResultSet

import com.softegra.common.DremioInit
import ru.yandex.clickhouse.{ClickHouseConnectionImpl, ClickHouseDataSource}

import scala.collection.immutable.HashMap

object DremioUtil {
  def results[T](resultSet: ResultSet)(f: ResultSet => T) = {
    new Iterator[T] {
      def hasNext: Boolean = resultSet.next()

      def next() = f(resultSet)
    }
  }


  def rsToStream(rs: ResultSet): List[HashMap[String, Any]] = {
    val columns = rs.getMetaData
    val it = results(rs) {
      case rs =>
        var mn = new HashMap[String, Any]
        for (i <- 1 to columns.getColumnCount) {
          mn += columns.getColumnName(i) -> rs.getObject(i)
        }
        mn
    }
    it.toList
  }

//  def executeSQL(sql: String): Stream[HashMap[String, Any]] = {
//    val statement = DremioInit.connect().createStatement()
//    val rs = statement.executeQuery(sql)
//    rsToStream(rs)
//  }

  def executeSQL(sql: String): List[HashMap[String, Any]] = {
    val statement = DremioInit.connect().createStatement()
    val rs = statement.executeQuery(sql)
    rsToStream(rs)
  }

}
