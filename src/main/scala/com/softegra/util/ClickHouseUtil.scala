package com.softegra.util

import java.io.{BufferedReader, File, FileOutputStream, InputStreamReader}
import java.net.URL
import java.sql.{Date, ResultSet}
import java.util.{Calendar, Properties}

import com.jcraft.jsch.{ChannelExec, Session}
import org.apache.commons.io.IOUtils
import org.apache.commons.vfs2.{FileSystemOptions, Selectors}
import org.apache.commons.vfs2.impl.StandardFileSystemManager
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder
import ru.yandex.clickhouse.{ClickHouseConnectionImpl, ClickHouseDataSource}

import scala.collection.immutable.HashMap

object ClickHouseUtil {
  //date
  val cal = Calendar.getInstance()
  val dat = new Date(cal.getTimeInMillis - (1000 * 60 * 60 * 24)).toString
  val year = dat.substring(0,4)
  val month = dat.substring(5,7)
  val day = dat.substring(8,10)

  //database clickHouse
//  val dataSource = new ClickHouseDataSource("jdbc:clickhouse://192.168.3.124:8123")
  val dataSource = new ClickHouseDataSource("jdbc:clickhouse://10.53.193.102:8123")

  def results[T](resultSet: ResultSet)(f: ResultSet => T) = {
    new Iterator[T] {
      def hasNext: Boolean = resultSet.next()

      def next() = f(resultSet)
    }
  }

  def rsToStream(rs: ResultSet): List[HashMap[String, Any]] = {
    val columns = rs.getMetaData
    val it = results(rs) {
      case rs =>
        var mn = new HashMap[String, Any]
        for (i <- 1 to columns.getColumnCount) {
          mn += columns.getColumnName(i) -> rs.getObject(i)
        }
        mn
    }
    it.toList
  }

  def insertQuery(stringQuery: String, nameDB : String): Unit = {
    val sql = s"INSERT INTO ${nameDB} VALUES $stringQuery"
    val connection = dataSource.getConnection.asInstanceOf[ClickHouseConnectionImpl]
    connection.setCatalog("system")
    val statement = connection.createStatement
    connection.setCatalog("default")
    statement.executeQuery(sql)
    statement.close()
    connection.close()
  }

  def selectQuery(sql: String): List[HashMap[String, Any]] ={
    val connection = dataSource.getConnection.asInstanceOf[ClickHouseConnectionImpl]
    connection.setCatalog("system")
    val statement = connection.createStatement
    connection.setCatalog("default")
    statement.executeQuery(sql)
    val resultSet = statement.executeQuery(sql)
    val ret = rsToStream(resultSet)
    statement.close()
    connection.close()
    ret
  }

  def anyQuery(sql: String): Unit ={
    val connection = dataSource.getConnection.asInstanceOf[ClickHouseConnectionImpl]
    connection.setCatalog("system")
    val statement = connection.createStatement
    connection.setCatalog("default")
    statement.executeQuery(sql)
  }

  def ftpDownload(ftpIp: String, user: String, pass: String,saveTo: String, fromFile: String): Unit ={
    val dl = new URL(s"ftp://$user:$pass@$ftpIp"+fromFile)
    println(dl)
    val fl = new File(saveTo)
    val os = new FileOutputStream(fl)
    val is = dl.openStream
    println(s"file ${fromFile} being start to download")
    IOUtils.copy(is, os)
    println(s"file ${fl.getName} successfully downloaded")
  }

  def downloadFTP(address: String, user: String, pass: String, remoteDir: String, localDir: String, fileToDownload: String): Unit = {


    val props = new Properties
    val manager = new StandardFileSystemManager
    try {
      //        props.load(new FileInputStream(propertiesFilename))
      val serverAddress = address
      val userId =user
      val password = pass
      val remoteDirectory = remoteDir
      val localDirectory = localDir
      //Initializes the file manager
      manager.init()
      //Setup our SFTP configuration
      val opts = new FileSystemOptions
      SftpFileSystemConfigBuilder.getInstance.setStrictHostKeyChecking(opts, "no")
      SftpFileSystemConfigBuilder.getInstance.setUserDirIsRoot(opts, true)
      SftpFileSystemConfigBuilder.getInstance.setTimeout(opts, 100000)
      //Create the SFTP URI using the host name, userid, password,  remote path and file name
      val sftpUri = "sftp://" + userId + ":" + password + "@" + serverAddress + "/" + remoteDirectory + fileToDownload
      // Create local file object
      val filepath = localDirectory + fileToDownload
      val file = new File(filepath)
      val localFile = manager.resolveFile(file.getAbsolutePath)
      // Create remote file object
      val remoteFile = manager.resolveFile(sftpUri, opts)
      // Copy local file to sftp server
      localFile.copyFrom(remoteFile, Selectors.SELECT_SELF)
      //      System.out.println("File "+ fileToDownload +" downloaded successful")
    } catch {
      case ex: Exception =>
        ex.printStackTrace()
        return false
    } finally manager.close()
  }

  def run2(session: Session, command: String, fn: (String) => Any): Unit = {
    try {
      val channel = session.openChannel("exec")
      channel.asInstanceOf[ChannelExec].setCommand(command)
      channel.setInputStream(null)
      channel.asInstanceOf[ChannelExec].setErrStream(System.err)
      val in = channel.getInputStream
      channel.connect()

      val br = new BufferedReader(new InputStreamReader(in))

      Stream.continually(br.readLine()).takeWhile(_ != -1).takeWhile(_ != null).foreach { str =>
        fn(str)
      }

      br.close()
      channel.disconnect()
    }
    catch {
      case e: Exception => e.printStackTrace()
    }
  }
}
