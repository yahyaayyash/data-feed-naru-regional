package com.softegra.util

/**
  * Created by fat on 12/17/18.
  */
import java.io.{BufferedReader, InputStreamReader}
import java.util.Properties

import com.jcraft.jsch._
import org.apache.commons.vfs2.FileSystemOptions
import org.apache.commons.vfs2.impl.StandardFileSystemManager
import org.apache.commons.vfs2.provider.sftp.SftpFileSystemConfigBuilder

import scala.collection.mutable.ListBuffer
import util.control.Breaks._
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

object SftpUtil {
  val timebreak = "00-20"

  def checkBreak(): Boolean = {
    var bool = false
    while (!bool) {
      val now = new java.text.SimpleDateFormat("HH-mm").format(new java.util.Date(System.currentTimeMillis()))
      Thread.sleep(1000 * 15)
      if (now.contains(timebreak)) bool = true
    }
    bool
  }

  def run(session: Session, command: String, fn: (String) => Any): Unit = {
    try {
      val channel = session.openChannel("exec")
      channel.asInstanceOf[ChannelExec].setCommand(command)
      channel.setInputStream(null)
      channel.asInstanceOf[ChannelExec].setErrStream(System.err)
      val in = channel.getInputStream
      channel.connect()

      val br = new BufferedReader(new InputStreamReader(in))

      new Thread {
        override def run {
          val check = checkBreak()
          if (check) {
            channel.disconnect()
            br.close()
            break()
          }
        }
      }.start()

      Stream.continually(br.readLine()).takeWhile(_ != -1).takeWhile(_ != null).foreach { str =>
        fn(str)
      }

      br.close()
      channel.disconnect()
    }
    catch {
      case e: Exception => e.printStackTrace()
    }
  }

  def run2(session: Session, command: String, fn: (String) => Any): Unit = {
    try {
      val channel = session.openChannel("exec")
      channel.asInstanceOf[ChannelExec].setCommand(command)
      channel.setInputStream(null)
      channel.asInstanceOf[ChannelExec].setErrStream(System.err)
      val in = channel.getInputStream
      channel.connect()

      val br = new BufferedReader(new InputStreamReader(in))

      Stream.continually(br.readLine()).takeWhile(_ != -1).takeWhile(_ != null).foreach { str =>
        fn(str)
      }

      br.close()
      channel.disconnect()
    }
    catch {
      case e: Exception => e.printStackTrace()
    }
  }

  def listFileFTP(address: String, user: String, pass: String, remoteDir: String): ListBuffer[String] = {

    val ls = new ListBuffer[String]

    val props = new Properties
    val manager = new StandardFileSystemManager
    try {
      //        props.load(new FileInputStream(propertiesFilename))
      val serverAddress = address
      val userId = user
      val password = pass
      val remoteDirectory = remoteDir
      //Initializes the file manager
      manager.init()
      //Setup our SFTP configuration
      val opts = new FileSystemOptions
      SftpFileSystemConfigBuilder.getInstance.setStrictHostKeyChecking(opts, "no")
      SftpFileSystemConfigBuilder.getInstance.setUserDirIsRoot(opts, true)
      SftpFileSystemConfigBuilder.getInstance.setTimeout(opts, 100000)
      //Create the SFTP URI using the host name, userid, password,  remote path and file name
      val sftpUri = "sftp://" + userId + ":" + password + "@" + serverAddress + "/" + remoteDirectory
      val remoteFile = manager.resolveFile(sftpUri, opts)
      val child = remoteFile.getChildren
      for (children <- child) {
        ls += children.getName.getBaseName
      }
    } catch {
      case ex: Exception =>
        ex.printStackTrace()
        return ls
    } finally manager.close()
    ls
  }

}