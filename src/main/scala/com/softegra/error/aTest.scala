package com.softegra.error
import java.io.{File, FileOutputStream, IOException, PrintWriter}
import java.net.URL

import com.softegra.util.{ClickHouseUtil, DremioUtil}
import org.apache.commons.io.IOUtils
import org.apache.commons.net.ftp.FTPClient
import org.apache.commons.net.ftp.FTPReply

import scala.collection.mutable.ListBuffer
import scala.io.Source

object aTest extends App {

//  ftpDownloadRoaming()
  test4()


  def test1 (): Unit = {
    val sql =
      """
        |SELECT *
        |FROM "@admin".naru2018.voice_2g
      """.stripMargin
    val resultSet = DremioUtil.executeSQL(sql)
    val a = resultSet.map(x => {
      if(x("datetime").toString == ""){
        "betul"
      } else {
        x("datetime")
      }
    }).distinct
    println(a)
  }

  def ftpDownloadRoaming(saveTo: File, fromFile: String): Unit ={
    val dl = new URL("ftp://appsroaming:roaming123@10.251.166.147/"+fromFile)
    val fl = saveTo

    val os = new FileOutputStream(fl)
    val is = dl.openStream
    println(s"file ${fromFile} being start to download")
    IOUtils.copy(is, os)
    println(s"file ${saveTo.getName} successfully downloaded")
  }

  def listDirectory(ftpClient: FTPClient, parentDir: String, currentDir: String, level: Int): Unit = {
      println("mulai listing")
      var dirToList = parentDir
      println(s"dirToList : $dirToList")
      if (!(currentDir == "")) dirToList += "/" + currentDir
      val subFiles = ftpClient.listFiles(dirToList)
      println(s"dirToList : $dirToList, size : ${subFiles.size}")
      if (subFiles != null && subFiles.length > 0) for (aFile <- subFiles) {
        println("file  ==> "+aFile)
        val currentFileName = aFile.getName
        if (currentFileName == "." || currentFileName == "..") { // skip parent directory and directory itself
           //todo: continue is not supported
        }
        var i = 0
        while ( {
          i < level
        }) {
          System.out.print("\t")

          {
            i += 1; i - 1
          }
        }
        if (aFile.isDirectory) {
          System.out.println("[" + currentFileName + "]")
          listDirectory(ftpClient, dirToList, currentFileName, level + 1)
        }
        else System.out.println(currentFileName)
      }
    }

  val l = new ListBuffer[String]()
//  test3(l)
  def test3(l : ListBuffer[String]): Unit ={
    l.append("nambah")
  }
//  println(l)


  def nioAddAreaTime(): Unit = {
    val pw = new PrintWriter(new File("E:\\Ayyash\\NARU 2018\\Niometric\\supoi_fix.csv"))
    val file = "E:\\Ayyash\\NARU 2018\\Niometric\\supoi2.csv"
    for(line <- Source.fromFile(file).getLines().toList){
      if(!line.contains("LOCATION")){
        if(line.contains("MEGA MALL") || line.contains("PANTAI KUTA")){
          pw.write(line+",WITA\n")
        }
        else if(line.contains("LAPANGAN MERDEKA AMBON")) {
          pw.write(line+",WIT\n")
        }
        else {
          pw.write(line+",WIB\n")
        }
      } else {
        pw.write(line+",TIME_AREA\n")
      }
    }
    pw.close()
  }

//  def test4(): Unit ={
//    for(i <- 1 to 7){
//      println(i)
//    }
//  }

  def test4(): Unit ={
            val old = List(1,2,3)
            val news = List(2,3,4)
    val res = old.map(x => {
      if(!news.contains(x)){
        x
      } else {
        0
      }
    })
    println(res.filter(_ != 0))
  }

  def delTimestampList(old: ListBuffer[String], news: List[String]): List[String] ={
    val res = news.map(x => {
      if(!old.contains(x)){
        x
      } else {
        ""
      }
    })
    res.filter(_ != "").toList
  }
}
