//package com.softegra.error;
//
//import org.apache.commons.net.PrintCommandListener;
//import org.apache.commons.net.ftp.FTPClient;
//import org.apache.commons.net.ftp.FTPReply;
//
//import java.io.File;
//import java.io.IOException;
//import java.io.InputStream;
//import java.io.PrintWriter;
//import java.net.URL;
//import java.net.URLConnection;
//import java.nio.file.Files;
//import java.util.Collection;
//
//class FtpClient {
//
//    private String server;
//    private int port;
//    private String user;
//    private String password;
//    private FTPClient ftp;
//
//    // constructor
//
//    void open() throws IOException {
//        ftp = new FTPClient();
//
//        ftp.addProtocolCommandListener(new PrintCommandListener(new PrintWriter(System.out)));
//
//        ftp.connect(server, port);
//        int reply = ftp.getReplyCode();
//        if (!FTPReply.isPositiveCompletion(reply)) {
//            ftp.disconnect();
//            throw new IOException("Exception in connecting to FTP Server");
//        }
//
//        ftp.login(user, password);
//    }
//
//    void close() throws IOException {
//        ftp.disconnect();
//    }
//
////    public void givenRemoteFile_whenListingRemoteFiles_thenItIsContainedInList() throws IOException {
////        Collection<String> files = ftp.listFiles("");
////        assertThat(files).contains("foobar.txt");
////    }
//
//    public void givenRemoteFile_whenDownloading_thenItIsOnTheLocalFilesystem() throws IOException {
//        String ftpUrl = String.format(
//                "ftp://user:password@localhost:%d/foobar.txt", fakeFtpServer.getServerControlPort());
//
//        URLConnection urlConnection = new URL(ftpUrl).openConnection();
//        InputStream inputStream = urlConnection.getInputStream();
//        Files.copy(inputStream, new File("downloaded_buz.txt").toPath());
//        inputStream.close();
//
//        assertThat(new File("downloaded_buz.txt")).exists();
//
//        new File("downloaded_buz.txt").delete(); // cleanup
//    }
//}