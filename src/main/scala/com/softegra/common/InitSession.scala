package com.softegra.common

import com.jcraft.jsch.{JSch, Session, UserInfo}

object InitSession {
  def init(host: String, user: String, password: String): Session = {
    val info = new UserInfo() {
      override def getPassphrase: String = null

      override def getPassword: String = password

      override def promptPassword(message: String): Boolean = {
        System.out.println(message)
        true
      }

      override def promptPassphrase(message: String) = true

      override def promptYesNo(message: String): Boolean = {
        System.out.println(message)
        true
      }

      override def showMessage(message: String): Unit = {
        System.out.println(message)
      }
    }
    val jsch = new JSch
    jsch.addIdentity("/home/ariussub/.ssh/id_rsa")
//    jsch.addIdentity("/Users/a_braincode/.ssh/id_rsa")
    val session = jsch.getSession(user, host, 22)
    session.setUserInfo(info)
    session.connect()
    session
  }
}
