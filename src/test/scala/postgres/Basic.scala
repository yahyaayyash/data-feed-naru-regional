package postgres

import com.softegra.domain.Users
import com.softegra.util.{PostgresUtil, QueryUtil}
import com.twitter.finagle.Postgres
import com.twitter.util.Await

/**
  * Created by subhan on 1/14/17.
  */
object Basic extends App {

  case class Customers(id: Int, firstname: String, lastname: String)

    val client = Postgres.Client()
      .withCredentials("subhan", Some(""))
      .database("finatra1")
      .withSessionPool.maxSize(1)
      .withBinaryResults(true)
      .withBinaryParams(true)
      //    .withTransport.tls("host")
      .newRichClient("localhost:5432")

//  val users =  Await.result {
//    QueryUtil.list[Users]().map(x => x.map(y => y.listRules.map(z => println(s"rule -> ${z}"))))
//    QueryUtil.list[Customers]()
//    PostgresUtil.list("select * from users")
//  }
//  println(users)

  val test = new Customers(1,"Subhan","Fauzi")
  QueryUtil.update[Customers]( test, "firstname", "Mujahid")



}
