package json

import com.twitter.finatra.json.FinatraObjectMapper

/**
  * Created by subhan on 10/15/16.
  */
object AllTypes extends App {

  case class CaseClassWithAllTypes(
                                    map: Map[String, String],
                                    set: Set[Int],
                                    string: String,
                                    list: List[Int],
                                    seq: Seq[Int],
                                    indexedSeq: IndexedSeq[Int],
                                    vector: Vector[Int],
                                    bigDecimal: BigDecimal,
                                    bigInt: Int, //TODO: BigInt,
                                    int: Int,
                                    long: Long,
                                    char: Char,
                                    bool: Boolean,
                                    short: Short,
                                    byte: Byte,
                                    float: Float,
                                    double: Double,
                                    any: Any,
                                    anyRef: AnyRef,
                                    intMap: Map[Int, Int] = Map(),
                                    longMap: Map[Long, Long] = Map())

  case class CaseClassWithException() {
    throw new NullPointerException("Oops!!!")
  }


  val json = """
               {
                 "map": {
                   "one": "two"
                 },
                 "set": [1, 2, 3],
                 "string": "woo",
                 "list": [4, 5, 6],
                 "seq": [7, 8, 9],
                 "sequence": [10, 11, 12],
                 "collection": [13, 14, 15],
                 "indexed_seq": [16, 17, 18],
                 "random_access_seq": [19, 20, 21],
                 "vector": [22, 23, 24],
                 "big_decimal": 12.0,
                 "big_int": 13,
                 "int": 1,
                 "long": 2,
                 "char": "x",
                 "bool": false,
                 "short": 14,
                 "byte": 15,
                 "float": 34.5,
                 "double": 44.9,
                 "any": true,
                 "any_ref": "wah"
               }"""
  /* TODO
     "intMap": {
       "1": "1"
     },
     "longMap": {
       "2": 2
     }
   }
   """
   */

  val mapper = FinatraObjectMapper.create()
  private def parse[T: Manifest](string: String): T = {
    mapper.parse[T](string)
  }

  val got = parse[CaseClassWithAllTypes](json)
  val expected = CaseClassWithAllTypes(
    map = Map("one" -> "two"),
    set = Set(1, 2, 3),
    string = "woo",
    list = List(4, 5, 6),
    seq = Seq(7, 8, 9),
    indexedSeq = IndexedSeq(16, 17, 18),
    vector = Vector(22, 23, 24),
    bigDecimal = BigDecimal("12.0"),
    bigInt = 13, //todo
    int = 1,
    long = 2L,
    char = 'x',
    bool = false,
    short = 14,
    byte = 15,
    float = 34.5f,
    double = 44.9d,
    any = true,
    anyRef = "wah"
    //intMap = Map(1 -> 1), //TODO
    //longMap = Map(2L -> 2L) //TODO
  )

  println(s"got -> ${got}")
  println(s"expected -> ${expected}")


}
