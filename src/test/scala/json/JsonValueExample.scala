package json

import com.fasterxml.jackson.annotation.JsonValue
import com.twitter.finatra.json.FinatraObjectMapper

/**
  * Created by subhan on 10/15/16.
  */
object JsonValueExample extends App {

  sealed trait CarType {
    @JsonValue
    def toJson: String
  }

  object Audi extends CarType {
    override def toJson: String = "audi"
  }

  case class Vehicle(vin: String, `type`: CarType)

  val vehicle = Vehicle("vin -1", Audi)

  val mapper = FinatraObjectMapper.create()
  val serialization = mapper.writeValueAsString(vehicle)
  println(s"serialization -> ${serialization}")

}
