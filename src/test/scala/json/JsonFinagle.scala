package json

import com.twitter.finatra.json.FinatraObjectMapper


/**
  * Created by subhan on 10/15/16.
  */
object JsonFinagle extends App {

  val mapper = FinatraObjectMapper.create()
  private def parse[T: Manifest](string: String): T = {
    mapper.parse[T](string)
  }


  case class Person(
                     id: Int,
                     name: String,
                     age: Option[Int],
                     age_with_default: Option[Int] = None,
                     nickname: String = "unknown"
                   )


  val steve = Person(
    id = 1,
    name = "Steve",
    age = Some(20),
    age_with_default = Some(20),
    nickname = "ace")

  val steveJson =
    """{
         "id" : 1,
         "name" : "Steve",
         "age" : 20,
         "age_with_default" : 20,
         "nickname" : "ace"
       }
    """

  val json = Seq(steveJson, steveJson).mkString("[", ", ", "]")
  val persons = parse[Seq[Person]](json)

  println(s"json -> ${json}")
  println(s"persons -> ${persons}")

}
