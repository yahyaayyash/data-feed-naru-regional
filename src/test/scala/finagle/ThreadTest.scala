package finagle

object ThreadTest extends App {

  println("ini thread utama")

  new Thread(new MyThread()).start()
  Thread.sleep(2000)
  println("sdh di akhir")
  class MyThread extends Runnable {
    def run: Unit = {
      println("di dalam thread")
    }
  }
}


