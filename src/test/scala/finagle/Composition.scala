package finagle

import com.twitter.util.Future

/**
  * Consider the simple example of fetching a representative thumbnail from a website (ala Pinterest). This typically involves:
  * *
  * Fetching the homepage
  * Parsing that page to find the first image link
  * Fetching the image link
  * This is an example of sequential composition: in order to do the next step, we must have successfully completed the previous one.
  * With Futures, this is called flatMap [3]. The result of flatMap is a Future representing the result of this composite operation.
  * Given some helper methods — fetchUrl fetches the given URL, findImageUrls parses an HTML page to find image links
  * — we can implement our Pinterest-style thumbnail extract like this:
  */
object Composition {

  def fetchUrl(url: String): Future[Array[Byte]] = ???

  def findImageUrls(bytes: Array[Byte]): Seq[String] = ???

  val url = "http://www.google.com"

  val f: Future[Array[Byte]] = fetchUrl(url).flatMap { bytes =>
    val images = findImageUrls(bytes)
    if (images.isEmpty)
      Future.exception(new Exception("no image"))
    else
      fetchUrl(images.head)
  }

  f.onSuccess { image =>
    println("Found image of size " + image.size)
  }

  //cara lain
  val collected: Future[Seq[Array[Byte]]] =
    fetchUrl(url).flatMap { bytes =>
      val fetches = findImageUrls(bytes).map { url => fetchUrl(url) }
      Future.collect(fetches)
    }
}
