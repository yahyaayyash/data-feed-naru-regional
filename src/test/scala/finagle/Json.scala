package finagle

import com.fasterxml.jackson.core.{JsonFactory, JsonParser, JsonProcessingException}
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.experimental.ScalaObjectMapper
import com.twitter.concurrent.AsyncStream
import com.twitter.io.{Buf, Reader}

import scala.collection.mutable

object Json {
  private[this] val mapper = new ObjectMapper with ScalaObjectMapper
  private[this] lazy val factory: JsonFactory = mapper.getFactory

  protected[this] def parse[T](buf: Buf)(f: JsonParser => T) = {
    val Buf.ByteArray.Owned(bytes, begin, end) = Buf.ByteArray.coerce(buf)
    val parser = mapper.getFactory.createParser(bytes, begin, end - begin)
    try f(parser) finally parser.close()
  }

  private[this] object Incomplete {
    val unexpectedEOI = "Unexpected end-of-input"
    def unapply(jpe: JsonProcessingException): Boolean =
      jpe.getMessage match {
        case null => false
        case msg => msg.startsWith(unexpectedEOI)
      }
  }
  /**
    * Given a chunk of bytes, read a stream of objects, and return the remaining unread buffer.
    */
  def readChunked[T: Manifest](chunk: Buf): (Seq[T], Buf) = {
    var objs = mutable.Buffer.empty[T]
    var offset = 0L
    parse(chunk) { json =>
      var reading = true
      while (reading) {
        try {
          json.readValueAs(classManifest[T].erasure) match {
            case obj: T if obj != null =>
              objs.append(obj)
              offset = json.getCurrentLocation.getByteOffset
              reading = offset < chunk.length
            case _ =>
              val Buf.Utf8(chunkstr) = chunk
              throw new IllegalStateException(
                s"could not decode json object in chunk @ ${offset} bytes: ${chunkstr}"
              )
          }
        } catch {
          case Incomplete() =>
            reading = false
        }
      }
    }
    val rest = chunk.slice(offset.toInt, chunk.length)
    (objs, rest)
  }

  def readStream[T: Manifest](reader: Reader, bufsiz: Int = 8 * 1024): AsyncStream[T] = {
    def chunks(init: Buf): AsyncStream[T] =
      for {
        read <- AsyncStream.fromFuture(reader.read(bufsiz))
        buf <- AsyncStream.fromOption(read)
        item <- {
          val (items, tail) = readChunked[T](init concat buf)
          AsyncStream.fromSeq(items) concat chunks(tail)
        }
      } yield item

    chunks(Buf.Empty)
  }
}