package finagle

import com.twitter.finagle.{Http, Service, http}
import com.twitter.util.{Await, Future}

/**
  * Created by subhan on 10/1/16.
  */
object MyFInagleTest extends App {
  val service = new Service[http.Request, http.Response] {
    def apply(req: http.Request): Future[http.Response] =
      Future.value {
        val resp = http.Response(req.version, http.Status.Ok)
        resp.setContentString("Ini test awal")
        resp
      }
  }
  val server = Http.serve(":8083", service)
  Await.ready(server)
}
