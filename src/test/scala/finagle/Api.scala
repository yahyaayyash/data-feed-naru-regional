package finagle

import com.twitter.concurrent.AsyncStream
import com.twitter.finagle.Service
import com.twitter.finagle.http.{Request, Response, Status}
import com.twitter.finagle.http
import com.twitter.util.Future

case class WatchEvent(kind: String)

private[this] case class UnexpectedResponse(rsp: http.Response) extends Throwable


class Api(client: Service[Request, Response]) {

  def mkreq(): Request = ???

  def watch(): AsyncStream[WatchEvent] =
    for {
      rsp <- AsyncStream.fromFuture(client(mkreq()))
      watch <- rsp.status match {
        case Status.Ok =>
          Json.readStream[WatchEvent](rsp.reader)
        case _ =>
          AsyncStream.fromFuture(
            Future.exception(UnexpectedResponse(rsp)))
      }
    } yield watch
}
