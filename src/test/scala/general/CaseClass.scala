package general

/**
  * Created by subhan on 1/4/17.
  */
object CaseClass extends App {
  case class PriceMove(price: Double, delta: Double)

  import Implicits._
  println(PriceMove(1.23, 2.56).toStringWithFields)

}


object Implicits {
  implicit class CaseClassToString(c: AnyRef) {
    def toStringWithFields: String = {
      val fields = (Map[String, Any]() /: c.getClass.getDeclaredFields) { (a, f) =>
        println(s"c -> ${c}")
        f.setAccessible(true)
        a + (f.getName -> f.get(c))
      }

      s"${c.getClass.getName}(${fields.mkString(", ")})"
    }
  }
}