package general

import java.util.Date

/**
  * Created by subhan on 1/4/17.
  */
object CaseClass2 extends App {

  case class PriceMove(name: String, price: Double, delta: Double, expireDate: Option[Date]=None)

  import scala.reflect.runtime.universe._

//  typeOf[PriceMove].members.filter(!_.isMethod).toSeq.reverse.foreach { prop =>
//    println(s"${prop.name} -> ${prop.typeSignature}")
//  }

  val mapNameToType = (Map[String, Type]() /: typeOf[PriceMove].members.filter(!_.isMethod).toSeq.reverse) { (map, prop) =>
    map + (prop.name.toString -> prop.typeSignature)
  }
  println(mapNameToType)

//  typeOf[PriceMove].members.filter(!_.isMethod).map(_.typeSignature).foreach {
//    tipe => println(tipe)
////    case t if t == typeOf[Int] => println("i")
////    case s if s == typeOf[String] => println("s")
////    case d if d == typeOf[Double] => println("d")
//  }

  val obj = PriceMove("Diskon", 1.23, 2.56)
  val cek = new CaseClassToString(obj)
  println(cek.toStringWithFields)

}


class CaseClassToString(c: AnyRef) {
  def toStringWithFields: String = {
    val fields = (Map[String, Any]() /: c.getClass.getDeclaredFields) { (a, f) =>
      f.setAccessible(true)
      a + (f.getName -> f.get(c))
    }

    s"${c.getClass.getName}(${fields.mkString(", ")})"
  }
}