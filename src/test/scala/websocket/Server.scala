package websocket

import com.twitter.concurrent.AsyncStream
import com.twitter.finagle.websocket.{Frame, Request, Response}
import com.twitter.finagle.{Service, Websocket}
import com.twitter.util.{Await, Future}

/**
  * Created by subhan on 10/1/16.
  */
object Server extends App {

  // A server that when given a number, responds with a word (mostly).
  def handler(messages: AsyncStream[Frame]): AsyncStream[Frame] = {
    messages.map {
      case Frame.Text("1") =>
        println("Frame 1")
        Frame.Text("one")
      case Frame.Text("2") =>
        println("Frame 2")
        Frame.Text("two")
      case Frame.Text("3") => Frame.Text("three")
      case Frame.Text("4") => Frame.Text("cuatro")
      case Frame.Text("5") => Frame.Text("five")
      case Frame.Text("6") => Frame.Text("6")
      case _ => Frame.Text("??")
    }
  }

  val server = Websocket.serve(":3005", new Service[Request, Response] {
    def apply(req: Request): Future[Response] =
      Future.value(Response(handler(req.messages)))
  })

  Await.ready(server)
}
