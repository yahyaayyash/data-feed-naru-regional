package websocket

import java.net.{InetSocketAddress, SocketAddress, URI}

import com.twitter.concurrent.AsyncStream
import com.twitter.conversions.time._
import com.twitter.finagle
import com.twitter.finagle.Service
import com.twitter.finagle.param.Stats
import com.twitter.finagle.stats.{NullStatsReceiver, StatsReceiver}
import com.twitter.finagle.util.DefaultTimer
import com.twitter.finagle.websocket.{Frame, Request, Response}
import com.twitter.util.{Await, Closable, Future}

/**
  * Created by subhan on 10/1/16.
  */
object Campur extends App {


  val echo = new Service[Request, Response] {
    def apply(req: Request): Future[Response] =
      Future.value(Response(req.messages))
  }

  connect(echo) { client =>

//    import java.io.File
//    val file = new File("/Users/subhan/Downloads/test-stream.txt")
//    val reader = Reader.fromFile(file)
//    val stream = AsyncStream.fromReader(reader).map(buf => Frame.Text(Buf.Utf8.unapply(buf).getOrElse("")))

    val frames = texts("hello", "world")
    for {
      response <- client(mkRequest("/", frames))
//      response <- client(Request(new URI("/"), Map.empty, new SocketAddress {}, stream))
      messages <- response.messages.toSeq()
    } yield {
      println(s"frames => ${frames}")
      println(s"message => ${messages} \n\n\n")
    }
  }


  implicit val timer = DefaultTimer.twitter

  def connect(service: Service[Request, Response], stats: StatsReceiver = NullStatsReceiver)
             (run: Service[Request, Response] => Future[Unit]): Unit = {
    val server = finagle.Websocket.server
      .withLabel("server")
      .configured(Stats(stats))
      .serve("localhost:*", service)

    val addr = server.boundAddress.asInstanceOf[InetSocketAddress]

    val client = finagle.Websocket.client
      .configured(Stats(stats))
      .newService(s"${addr.getHostName}:${addr.getPort}", "client")

    Await.result(run(client).ensure(Closable.all(client, server).close()), 999.second)
  }

  def texts(messages: String*): Seq[Frame] = messages.map(Frame.Text(_))


  def mkRequest(path: String, frames: Seq[Frame]): Request = {
    Request(new URI(path), Map.empty, new SocketAddress {}, AsyncStream.fromSeq(frames))
  }
}
