//package websocket
//
//import java.net.URL
//import java.util.Scanner
//
//import com.twitter.concurrent.AsyncStream
//import com.twitter.util.JavaTimer
//
//import scala.util.Random
////import com.twitter.finagle.http.{Request, Response, Status}
////import com.twitter.finagle.util.DefaultTimer
//import com.twitter.finagle.websocket.{Frame, Request, Response}
//import com.twitter.finagle.{Service, Websocket}
////import com.twitter.io.{Buf, Reader}
//import com.twitter.conversions.time._
//import com.twitter.util.{Await, Future}
//
////import scala.language.postfixOps
////import scala.language.implicitConversions
////import scala.concurrent.duration.DurationInt
//
//
///**
//  * Created by subhan on 10/1/16.
//  */
//object EchoServer extends App {
//
//  val random = new Random
//  implicit val timer = new JavaTimer
////  implicit val timer = DefaultTimer.twitter
//
//
//  val service_ = new Service[Request, Response] {
//
//    def apply(req: Request): Future[Response] = {
//      Future.value(Response(handler(req.messages)))
//    }
//  }
//
//  // Int, sleep, repeat.
//  def ints(): AsyncStream[Int] =
//    random.nextInt +::
//      AsyncStream.fromFuture(Future.sleep(1000.millis)).flatMap(_ => ints())
//
//  val service = new Service[Request, Response] {
//    // Only one stream exists.
//    @volatile private[this] var messages: AsyncStream[Frame] =
//      ints().map(n => Frame.Text("{\"name\":\"Sophie Beckham\",\"skills\":{\"android\":true,\"html5\":true,\"mac\":false,\"windows\":false,\"css\":false},\"address\":\"1197 Thunder Wagon Common, Cataract, RI, 02987-1016, US, (401) 747-0763\",\"years\":43,\"proficiency\":85,\"country\":\"Ireland\",\"continent\":\"Europe\",\"language\":\"English\",\"mobile\":\"+630 553 428 959\",\"landline\":\"+074 904 909 911\"}\n"))
//
//
//    // Allow the head of the stream to be collected.
//    messages.foreach(_ => messages = messages.drop(1))
//
//    def apply(request: Request) = {
////      val writable = Reader.writable()
////      // Start writing thread.
////      messages.foreachF(writable.write)
////      val cek = AsyncStream.fromReader(writable)
//      println(messages)
//      Future.value(Response(messages))
//    }
//  }
//
//  val server = Websocket.serve(":3005", service)
//
//  Await.ready(server)
//
//  /**
//    * This example shows a custom Observable that blocks
//    * when subscribed to (does not spawn an extra thread).
//    */
//  def customObservableBlocking(): Observable[String] = {
//    Observable(aSubscriber => {
//      for (i <- 0 to 50) {
//        if (!aSubscriber.isUnsubscribed) {
//          aSubscriber.onNext("value_" + i)
//        }
//      }
//      // after sending all values we complete the sequence
//      if (!aSubscriber.isUnsubscribed) {
//        aSubscriber.onCompleted()
//      }
//    })
//  }
//
//
//  /*
//  * Fetch a list of Wikipedia articles asynchronously.
//  */
//  def fetchWikipediaArticleAsynchronously(wikipediaArticleNames: String*): Observable[String] = {
//    Observable(subscriber => {
//      new Thread(new Runnable() {
//        def run() {
//          for (articleName <- wikipediaArticleNames) {
//            if (subscriber.isUnsubscribed) {
//              return
//            }
//            val url = "https://en.wikipedia.org/wiki/" + articleName
//            val art = new Scanner(new URL(url).openStream()).useDelimiter("\\A").next()
//            subscriber.onNext(art)
//          }
//          if (!subscriber.isUnsubscribed) {
//            subscriber.onCompleted()
//          }
//        }
//      }).start()
//    })
//  }
//
//  //  // A server that when given a number, responds with a word (mostly).
//  def handler(messages: AsyncStream[Frame]): AsyncStream[Frame] = {
//    println(s"messages -> ${messages}")
//    val temp = AsyncStream(Frame.Text("-apa"))
//    println(s"temp -> ${temp}")
//
//    temp.map {
//      case x =>
//        println(s"x1 -> ${x}")
//    }
//
//    val a = messages.++(temp)
//    //    Thread.sleep(1000)
//    //    val b = a.++(AsyncStream(Frame.Text("-saja")))
//    //    println(s"b -> ${b}")
//    a.map {
//      case x =>
//        println(s"x -> ${x}")
//        Frame.Text(x.toString)
//    }
//  }
//
//  //  def handler(messages: AsyncStream[Frame]): AsyncStream[Frame] =
//  //    messages.flatMap {
//  //      case Frame.Text(message) =>
//  //        // Print the received message.
//  //        println(message)
//  //
//  //        AsyncStream.fromFuture(
//  //          // Sleep for a second...
//  //          Future.sleep(1.second).map { _ =>
//  //            // ... and then send a message to the server.
//  //            Frame.Text(message)
//  //          })
//  //
//  //
//  //
//  //
//  //      case _ => AsyncStream.of(Frame.Text("??"))
//  //    }
//
//
//}
