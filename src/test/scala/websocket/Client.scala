package websocket

import java.net.URI

import com.twitter.concurrent.AsyncStream
import com.twitter.conversions.time._
import com.twitter.finagle.Websocket
import com.twitter.finagle.util.DefaultTimer
import com.twitter.finagle.websocket.{Frame, Request}
import com.twitter.util.{Future, Promise}


/**
  * Created by subhan on 10/1/16.
  */
object Client extends App {

  implicit val timer = DefaultTimer.twitter

  // Responds to messages from the server.
  def handler(messages: AsyncStream[Frame]): AsyncStream[Frame] =
    messages.flatMap {
      case Frame.Text(message) =>
        // Print the received message.
        println(s"message -> ${message}")

        AsyncStream.fromFuture(
          // Sleep for a second...
          Future.sleep(1.second).map { _ =>
            // ... and then send a message to the server.
            Frame.Text(message.length.toString)
          })

//        AsyncStream.fromSeq(Seq(Frame.Text("123")))

      case _ => AsyncStream.of(Frame.Text("??"))
    }

  val incoming = new Promise[AsyncStream[Frame]]
  val outgoing = Frame.Text("1") +:: handler(AsyncStream.fromFuture(incoming).flatten)
  println(s"outgoing -> ${outgoing.observe()}")

  val client = Websocket.client.newService(":3005")
  val req = Request(new URI("/"), Map.empty, null, outgoing)

  // Take the messages of the response and fulfill `incoming`.
  client(req).map(_.messages).proxyTo(incoming)

  Thread.sleep(50000)
}
